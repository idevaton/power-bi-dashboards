﻿using Microsoft.PowerBI.Api.Models;
using System;
using System.Collections.Generic;

namespace app.Models
{
    public class StockQuote
    {
        public string Symbol { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
    }

    public class AlertRequest
    {
        public string UserEmail { get; set; }
        public string UserCode { get; set; }
    }

    public class StockAlert
    {
        public string UserEmail { get; set; }
        public string UserLink { get; set; }
        public string AlertType { get; set; }
        public string Symbol { get; set; }
        public string Name { get; set; }
    }

    public class ReportEmbedConfig
    {
        // Report to be embedded
        public List<EmbedReport> EmbedReports { get; set; }

        // Embed Token for the Power BI report
        public EmbedToken EmbedToken { get; set; }
    }

    public class EmbedReport
    {
        // Id of Power BI report to be embedded
        public Guid ReportId { get; set; }

        // Name of the report
        public string ReportName { get; set; }

        // Embed URL for the Power BI report
        public string EmbedUrl { get; set; }
    }

    public class DashboardEmbedConfig
    {
        public Guid DashboardId { get; set; }

        public string EmbedUrl { get; set; }

        public EmbedToken EmbedToken { get; set; }
    }

    public class TileEmbedConfig
    {
        public Guid TileId { get; set; }

        public string EmbedUrl { get; set; }

        public EmbedToken EmbedToken { get; set; }

        public Guid DashboardId { get; set; }
    }

    public class PBIConfig
    {
        public string ApplicationId { get; set; }
        public Guid WorkspaceId { get; set; }
        public Guid ReportId { get; set; }
        public string ApplicationSecret { get; set; }
        public string Tenant { get; set; }
        public EmbedToken EmbedToken { get; set; }
        public string EmbedUrl { get; set; }
    }
}