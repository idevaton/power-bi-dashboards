﻿using sharsy.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace pbiembed.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Index(app.Models.PBIConfig model)
        {
            if(model != null && model.ApplicationId != null && model.WorkspaceId != null && model.ReportId != null && model.ApplicationSecret != null && model.Tenant != null && model.ApplicationId != null && model.ApplicationSecret != null)
            {
                try
                {
                    List<Guid> ids = new List<Guid>();
                    var embedResult = await EmbedService.GetEmbedParams(model.WorkspaceId, model.ReportId, ids, model.ApplicationId, model.ApplicationSecret, model.Tenant);
                    return View("Example", embedResult);
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message.ToString();
                    return View(model);
                }
            }
            else
            {
                TempData["error"] = "Error: Invalid input.";
            }
            return View(model);
        }
    }
}