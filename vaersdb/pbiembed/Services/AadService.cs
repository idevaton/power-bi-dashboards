﻿// ----------------------------------------------------------------------------
// Copyright (c) Microsoft Corporation.
// Licensed under the MIT license.
// ----------------------------------------------------------------------------

namespace sharsy.Services
{
    using Microsoft.Identity.Client;
    using System;
    using System.Configuration;
    using System.Threading.Tasks;

    public class AadService
    {
        private static readonly string m_authorityUrl = ConfigurationManager.AppSettings["authorityUrl"];
        private static readonly string[] m_scope = ConfigurationManager.AppSettings["scope"].Split(';');

        /// <summary>
        /// Get Access token
        /// </summary>
        /// <returns>Access token</returns>
        public async static Task<string> GetAccessToken(string applicationId, string applicationSecret, string tenant)
        {
            AuthenticationResult authenticationResult = null;
            //if (ConfigValidatorService.AuthenticationType.Equals("masteruser", StringComparison.InvariantCultureIgnoreCase))
            //{
            //    IPublicClientApplication clientApp = PublicClientApplicationBuilder
            //                                                        .Create(ConfigValidatorService.ApplicationId)
            //                                                        .WithAuthority(m_authorityUrl)
            //                                                        .Build();
            //    var userAccounts = await clientApp.GetAccountsAsync();

            //    try
            //    {
            //        authenticationResult = await clientApp.AcquireTokenSilent(m_scope, userAccounts.FirstOrDefault()).ExecuteAsync();
            //    }
            //    catch (MsalUiRequiredException)
            //    {
            //        SecureString secureStringPassword = new SecureString();
            //        foreach (var key in ConfigValidatorService.Password)
            //        {
            //            secureStringPassword.AppendChar(key);
            //        }
            //        authenticationResult = await clientApp.AcquireTokenByUsernamePassword(m_scope, ConfigValidatorService.Username, secureStringPassword).ExecuteAsync();
            //    }
            //}

            // Service Principal auth is recommended by Microsoft to achieve App Owns Data Power BI embedding
            if (ConfigValidatorService.AuthenticationType.Equals("serviceprincipal", StringComparison.InvariantCultureIgnoreCase))
            {
                // For app only authentication, we need the specific tenant id in the authority url
                var tenantSpecificURL = m_authorityUrl.Replace("organizations", tenant);

                IConfidentialClientApplication clientApp = ConfidentialClientApplicationBuilder
                                                                                .Create(applicationId)
                                                                                .WithClientSecret(applicationSecret)
                                                                                .WithAuthority(tenantSpecificURL)
                                                                                .Build();

                authenticationResult = await clientApp.AcquireTokenForClient(m_scope).ExecuteAsync();
            }

            return authenticationResult.AccessToken;
        }
    }
}
