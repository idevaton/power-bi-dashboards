var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();
//app.UseMiddleware<ApiKeyMiddleware>(); // enables authorization on all the calls

app.UseAuthorization();

app.MapControllers();

app.Run();

// References
// http://codingsonata.com/secure-asp-net-core-web-api-using-api-key-authentication/
// https://stackoverflow.com/questions/57467450/a-web-api-key-can-only-be-specified-when-a-web-api-key-name-is-provided
