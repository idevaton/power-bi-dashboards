﻿using System;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace XamarinAndPowerBI.ViewModels
{
    public class AboutViewModel : BaseViewModel
    {
        public HtmlWebViewSource VideoSource { get; set; }

        public AboutViewModel()
        {
            Title = "About";
            OpenWebCommand = new Command(async () => await Browser.OpenAsync("https://aka.ms/xamarin-quickstart"));

            VideoSource = new HtmlWebViewSource();
            VideoSource.Html = @"<html><body>  <div style=' position: relative; padding-bottom: 56.25%; padding-top: 25px;'> <iframe style='position: absolute; top: 0; left: 0; width: 100%; height: 100%;'  src='https://www.youtube.com/embed/bVdfj7HXuXE' frameborder='0' allowfullscreen></iframe></div> </body></html>";
        }

        public ICommand OpenWebCommand { get; }
    }
}