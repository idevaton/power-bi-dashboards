﻿using System.ComponentModel;
using Xamarin.Forms;
using XamarinAndPowerBI.ViewModels;

namespace XamarinAndPowerBI.Views
{
    public partial class ItemDetailPage : ContentPage
    {
        public ItemDetailPage()
        {
            InitializeComponent();
            BindingContext = new ItemDetailViewModel();
        }
    }
}