# Power BI Dashboards

- VAERS DB Dashboard: analyzing data from Vaccine Adverse Event Reporting System (VAERS)

## VAERS DB Dashboard Getting Started

- To see VAERS DB live dashboard visit this link: https://app.powerbi.com/view?r=eyJrIjoiZDI4N2QwMGMtNzUzZS00NGQwLWI3NWItZWY0ZTg2MWY4M2Q2IiwidCI6IjY4NjZkYzAwLWQ5YzYtNDIzYS1hYjg5LTFhMmJkMmUwZjVhYSIsImMiOjN9
- You can also clone this repo and open \power-bi-dashboards\vaersdb\index.html file (index.html consits of public "publish-to-web" and private dashboard "embedded report")
- NOTE: Publish-to-web doesn't display the Filters pane. If you're planning to publish a report to the web, consider adding slicers for filtering instead. https://docs.microsoft.com/en-us/power-bi/create-reports/power-bi-report-filter. It looks like publish-to-web also does not display individual tabs, instead just next and previous tabs arrows
- If you have Power BI Desktop installed you can download PBIT template file. For the data source you also need to download VAERS DB CSV file: https://vaers.hhs.gov/data.html (download raw data). Downloaded VAERS DB file should be named: 2021VAERSData.csv and then added as a data source to the PBIT report
- NOTE: due to GitLab file size limitations the PBIX and CSV files were not committed and are currently ignored
- Additional reference independent from this repo/info: https://stevekirsch.substack.com/p/new-vaers-analysis-reveals-hundreds

## Coming Soon

- Set up authentication via token or system user for embedded reports. Reference: https://docs.microsoft.com/en-us/power-bi/developer/embedded/embed-tokens
- Tutorial: https://docs.microsoft.com/en-us/power-bi/developer/embedded/embed-sample-for-customers?tabs=net-framework

# disclaimer

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
